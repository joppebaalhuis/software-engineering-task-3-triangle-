//
// Created by joppe on 8-10-2018.
#include "Triangle.h"
#define ERROR 4
#define EQUILATERAL 3
#define ISOSCELES 2
#define SCALENE 1


int Triangle::triangle_type(int side1, int side2, int side3)
{
    if((side1<=0) || (side2<=0) || (side2<=0))                      //check if for zero values
        return ERROR;

    if((side1==side2) || (side1==side3) || (side2==side3))
    {
        if((side1==side2) && (side2==side3))
            return EQUILATERAL;
        else
            return ISOSCELES;
    }

    return SCALENE;
}
//source https://stackoverflow.com/questions/20097110/test-a-function-that-returns-triangle-type