//
// Created by joppe on 2-10-2018.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "Triangle.h"
#include <exception>

#define ERROR 4
#define EQUILATERAL 3
#define ISOSCELES 2
#define SCALENE 1

using testing::Eq;

namespace
{
    //object creation
    Triangle triangleObj;
    TEST(test1,SCALENE )
    {
        EXPECT_EQ(SCALENE ,  triangleObj.triangle_type(10,30,20));
    }

    TEST(test2,ISOSCELES )
    {
        EXPECT_EQ(ISOSCELES ,  triangleObj.triangle_type(20,20,30));
    }

    TEST(test3,EQUILATERAL )
    {
        EXPECT_EQ(EQUILATERAL ,  triangleObj.triangle_type(20,20,20));
    }


    TEST(test4, ERROR)
    {
        EXPECT_EQ(ERROR, triangleObj.triangle_type(0,30,20));
        EXPECT_EQ(ERROR, triangleObj.triangle_type(20,0,20));
        EXPECT_EQ(ERROR, triangleObj.triangle_type(20,30,0));
    }

}

